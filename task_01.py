word = str(input())
import re


def is_palindrom(word):
    pattern = r'\,|\:|\.|\;|\-|\_'
    word = re.sub(pattern, '', word.lower())
    return list(word) == list(reversed(word))


print(is_palindrom(word))